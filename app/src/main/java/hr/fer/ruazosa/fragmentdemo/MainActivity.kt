package hr.fer.ruazosa.fragmentdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addRemoveFragmentButton.setOnClickListener {
            val myFragment = supportFragmentManager.findFragmentByTag("myFragmentTag")

            if (myFragment == null) {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.add(fragmentHolderView.id, HelloWorldFromFragment(), "myFragmentTag")
                transaction.commit()
            }
            else {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.remove(myFragment)
                transaction.commit()
            }

        }

    }

}
